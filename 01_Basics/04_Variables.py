# Correcto
nombre = "César"

Nombre = "Thor"

nOmbre_1 = "Jim"

anio = 1981
anioActual = 2017

edad = anioActual - anio

print(edad)

# Incorrecto
# print(nombre + " " + edad)
# 1nombre = "Mi Nombre"

# Correcto
print(nombre + " " + str(edad) + " años")
