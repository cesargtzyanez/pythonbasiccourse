word = "Rock and roll all night and party everyday!"

# The next lines are not well formatted
print( word )
print( len( word) )

print( word[0] )
print( word[3] )

print( word[-3] )

print( word[0:3] )
print( word[:3] )
print( word[3:] )
print( word[3:3] )

print( word[-1:] )
print( word[:-1] )

# We can't read in 'reverse'
print( word[-1:3] )

print( word[3:-1] )

print( word[0:6:2] )
print( word[0::2] )

# A better example
phoneNumber = "1, 2, 3, 4, 5, 6, 7, 8, 9, 0"
print( phoneNumber[0::3] )

# A not very useful example
print( "Hello " "everybody!" )

print( "Hey! " * 3 )

print( "rock" in word )
print( "Rock" in word )

numbers = "0123456789"
count = " mississippi".join(numbers)

print(count)