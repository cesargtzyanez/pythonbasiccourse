myAge = 36

myName = "César"

# Solution 1
print("My name is: " + myName + " and I'm " + str(myAge) + " years old.")

# Solution 2
# Newer in Python 3, so we can reuse the values, cool right?
print("My name is: {0} and I'm {1} years old.".format(myName, myAge))

# If there is only one variable we can leave empty braces
print("I am {}".format(myName))

# Other example
verb = "is "
finalText = " years old."
age1 = 35
age2 = 57
age3 = 33
age4 = 28

# Multi-line strings
print("""
    And my family are:
    My Wife {0}{2}{1} 
    My Wife's Mom {0}{3}{1}
    My Mother {0}{3}{1}
    My Brother {0}{4}{1}
    And my cousin {0}{5}{1}
""".format(verb, finalText, age1, age2, age3, age4))

# Solution 3
print("My name is: César and I'm %d years old" % myAge )
print("My name is: %s and I'm %d %s" % (myName, myAge, finalText))

print("-->%6.3f" % (100/3))
print("-->%7.3f" % (100/3))

# Newer version
print("-->{0:20.11}".format(100/3))

# Optional exercise
# Use the other example variables and refactor the first and second solution

for i in range(1,10):
    print("%d --- %d ---- %d" %(i, i ** 2, i ** 3))

print("-------------------")
# Adding some padding
for i in range(1,10):
    print("{0:2} --- {1:4} ---- {2:4}".format(i, i ** 2, i ** 3))

print("-"*19)

for i in range(1, 10):
    print("{0:2} --- {1:<4} ---- {2:<4}".format(i, i ** 2, i ** 3))
# TODO: add a link for documentation about String Formatting
