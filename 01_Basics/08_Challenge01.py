# Create a program that asks teh user for his name and his birth year
# Then calculate and show the current age with the message: "User, you are X years old!"

# Crea un programa que pida al usuario nombre y año de nacimiento
# Después calcula y muestra su edad con el siguiente mensaje: "¡Usuario, tienes X años de edad!"

currentYear = 2017

question1 = input('What is your name?')

question2 = input("What is your birth year?")

age = currentYear - int(question2)

answer = "Hello " + question1 + ", you are " + str(age) + " years old."

print(answer)
