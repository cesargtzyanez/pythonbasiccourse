age = int( input('How old are you?') );

elections = True

# Sintax
if True:
    print("It's true!")

# Simple version of IF flow
if age > 18:
    print ("You can vote")
else:
    print ("You can't vote")

# Nested IF version
if age >= 18:
    print ("You can vote")
    if elections:
        print("And it's elections time!")
    else:
        print('But wait for the next year')
else:
    print ("You can't vote now, you need to wait {0} years".format(18-age))

# Using boolean operators
if age >= 18 and elections:
    print("You can vote and now is the time!")
else:
    print("You will need to wait for vote")

# Using boolean operators
if age < 18 or not elections:
    print("Sorry, you can't vote now")

# Ternary Operator.
print('-'*10 + 'Ternary Operator')

adult = age >= 18

state1 = "Adult" if adult else "Still a kid!"
print(state1)

state2 = ("Kid","Adult")[adult]
print(state2)
