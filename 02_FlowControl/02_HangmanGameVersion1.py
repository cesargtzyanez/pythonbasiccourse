text = "Cancun"

errorMessage = "Wrong!"

question = input("This word has {0} letters, can you guess it?".format( len(text)))

if( question in text ):
    print("OK, very well! My word has {0} in it, you won!".format(question))
else:
    print(errorMessage)

