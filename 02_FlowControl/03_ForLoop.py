
# Basic for loop

for i in range(-5,5):
    print("The value for i is: {0}".format(i))

# Using strings
for char in "Abracadabra":
    print("Give a {0}!".format(char))

# Multiplication examples
for i in range(0,11):
    print("===== Board of {0} ======".format(i))
    for j in range(0,11):
        print(" {0} x {1} = {2}".format(i, j, i*j))
    print("=========================\n")

# ToDo: make an example using break and continue in the for loop
