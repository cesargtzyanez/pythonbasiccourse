originalWord = "Hello"
word = originalWord
shots = 3
wordLenght = len(word)

print("Guess the magic word. Hint: the magic word has {} letters.".format(wordLenght))
print("You have {0} errors chances".format(shots))

while shots > 0 and len(word) > 0:
    question = input("Guess a letter:\n")
    if question in word:
        word = word.replace(question,'')
        print("\nGood for you! You need guess {0} letters more!\n".format(len(word)))
    else:
        shots -=1
        print("\nWrong! You have {0} more error chances\n".format(shots))

if shots == 0:
    print("Sorry you lost! The word was {0}".format(originalWord))
else:
    print("Hey! You win! The word was {0}".format(originalWord))


