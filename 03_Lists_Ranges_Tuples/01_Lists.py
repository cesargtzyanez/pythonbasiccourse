fruits = ["mango", "banana", "blackberry", "apple", "kiwi", "melon"]
vegetables = ["carrot", "broccoli", "perejil"]

vegetables.append("beans")
# vegetables.append(["beans", "avocado"])

veganFood = fruits + vegetables

print(fruits)
print(vegetables)

orderedVeganFood = sorted(veganFood)

print(veganFood.sort())
print(sorted(veganFood))

print(orderedVeganFood)

print(veganFood)

print(veganFood == orderedVeganFood)
print(sorted(veganFood) == orderedVeganFood)

# creating a list using the constructor
list_1 = []
list_2 = list()
print(list_1 == list_2)
print(list("Hello"))

numbers = [1, 2, 3, 4, 5]

anotherNumbers = numbers
# anotherNumbers = list(numbers)
# anotherNumbers = sorted(numbers, reverse=True)

anotherNumbers.sort(reverse=True)

print(numbers)
print(anotherNumbers)
print(numbers is anotherNumbers)
print(numbers == anotherNumbers)

list_a = [2, 4, 6, 8]
list_b = [0, 1, 3, 5, 7, ]

list_c = [list_a, list_b]

print(list_c)
# Mini challenge: show every item of the list of lists :)
