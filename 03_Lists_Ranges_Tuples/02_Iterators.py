numbers = "0123456789"

myIterator = iter(numbers)

print(myIterator)

# print (next(myIterator))
# print (next(myIterator))
# print (next(myIterator))
# print (next(myIterator))
# print (next(myIterator))
# print (next(myIterator))
# print (next(myIterator))
# print (next(myIterator))
# print (next(myIterator))
# print (next(myIterator))

# Error here! There are only 10 items
# print (next(myIterator))

for number in numbers:
    print(number)

for item in iter(numbers):
    print("my item {}".format(item))

for i in range(0, len(numbers)):
    print("My Iterator item: {}".format(next(myIterator)))
