myList = list(range(10))

print(myList)

even = list(range(0, 100, 2))
odd = list(range(1, 100, 2))

print(even)
print(odd)

decimals = range(0, 100)
print(decimals)

myRange = decimals[3:40:3]
print(myRange)

for i in myRange:
    print(i)

print("-" * 20)

for i in range(3, 40, 3):
    print(i)

print(myRange == range(3, 40, 3))

# Equivalent ranges
print(range(0, 5, 2) == range(0, 6, 2))

r = range(100)

print("-" * 20)
for i in r[::-2]:
    print(i)

print("-" * 20)
for i in range(99, 0, -2):
    print(i)

print("=" * 20)
for i in range(0, 100, -2):
    print(i)

print("=" * 20)
backMessage = "!nohtyP nrael ot evol I"
print(backMessage[::-1])

print("=" * 20)
fours = range(0, 100, 4)
print(list(fours))

fives = fours[::5]
print(list(fives))
