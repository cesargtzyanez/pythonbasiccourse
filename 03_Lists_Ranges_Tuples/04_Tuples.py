album1 = "Destroyer", "Kiss", 1976
album2 = "A Night At The Opera", "Queen", 1975
album3 = "Voodoo Lounge", "Rolling Stones", 1994
album4 = "Led Zeppelin IV", "Led Zeppelin", 1971

print(album1[0])
print(album2[1])
print(album3[2])
print(album4)

# Error
# album1[0] = "Destroyer"

# Correct
album1 = "Let It Be", "Beatles", 1970
print(album1)

print("\n")
for album in list([album1, album2, album3]):
    # Must be same number of variables than items
    # albumTitle, artist = album
    albumTitle, artist, year = album
    print("Album Title: {}".format(albumTitle))


# Challenge: create 3 different lists of Albums , Artists and years, using the previous lists :)

