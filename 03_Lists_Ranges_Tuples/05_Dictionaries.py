separator = "-" * 20 + "\n"
print("\nFirst Version of ALBUMS dictionary:\n")

albums = {
    "Destroyer": "Kiss",
    "A Night At The Opera": "Queen",
    "Voodoo Lounge": "Rolling Stones",
    "Led Zeppelin IV": "Led Zeppelin"
}

print(albums)
print(albums.keys())
print(albums.values())

print(albums.items())
# Error: seems like a list, but is not
# print(albums.items()[0])

# Now we convert to list, and they seem equal, but they are not
print(list(albums.items()))
print(list(albums.items())[0])

print(tuple(albums.items()))

print(albums["Destroyer"])
print(albums["A Night At The Opera"])

albums["Led Zeppelin IV"] = "Led Zep"
print(albums)

for item in albums:
    print("Album {}".format(albums[item]))

for key in albums.keys():
    print("Key {}".format(key))

for val in albums.values():
    print("Value {}".format(val))

# Mini-mini challenge, create a loop using tuples and lists

# Error: don't delete the dictionary
# del albums
albums.clear()
print(albums)

# Mini challenge: create a code that order and shows the key/value items of the dictionary
# hint: use a for loop and sort method

print(separator)

# Error
# print(albums[0])

print("\nSecond Version of ALBUMS dictionary:\n")
albumsV2 = {
    "01": {"Destroyer": "Kiss"},
    "02": {"A Night At The Opera": "Queen"},
    "03": {"Voodoo Lounge": "Rolling Stones"},
    "04": {"Led Zeppelin IV": "Led Zeppelin"}
}

print(albumsV2["01"])
print(albumsV2["01"]["Destroyer"])

print(separator)

print("Third Version of ALBUMS dictionary:\n")
albumsV3 = {
    "Kiss": {
        "Destroyer": {"year": 1976, "stock": 3},
        "Love Gun": {"year": 1977, "stock": 5},
        "Alive II": {"year": 1977, "stock": 1},
    },

    "Queen": {
        "A Night At The Opera": {"year": 1975, "stock": 5},
        "The Game": {"year": 1980, "stock": 4},
        "News Of The World": {"year": 1977, "stock": 7},
    },

    "Rolling Stones": {
        "Voodoo Lounge": {"year": 1994, "stock": 12},
        "Bridges To Babylon": {"year": 1997, "stock": 7},
        "Blue And Lonesome": {"year": 2016, "stock": 23},
    },

    "Led Zeppelin": {
        "Led Zeppelin ": {"year": 1969, "stock": 5},
        "Coda": {"year": 1982, "stock": 8},
        "Led Zeppelin II": {"year": 1969, "stock": 9},
    }
}

print(albumsV3["Kiss"]["Destroyer"]["stock"])
print(albumsV3["Kiss"]["Destroyer"]["year"])

print(albumsV3.get('Kiss'))
print(albumsV3.get('Destroyer'))
print(albumsV3.get('Destroyer', 'Sorry :('))
print(albumsV3['Kiss'].get('Destroyer'))
print(albumsV3['Kiss'].get('Destroyer').get('year'))

print(separator)

# Challenge: create a application for a store
print("Store Application:\n")
# band = input("What band do you want to hear?\n")
band = "jj"
if band in albumsV3:
    album = input("What album of {} do you want to hear?\n".format(band))
    if album in albumsV3[band]:
        year = albumsV3[band][album]["year"]
        stock = albumsV3[band][album]["stock"]
        message = "Oh, do you know that the album {} of {} was release in {}. Your're lucky we have {} in stock".format(
            album, band, year, stock)
    else:
        message = "Sorry,we don't have {}, but we have some others {} albums\n".format(album, band)
else:
    message = "Sorry, we don't have that band for now.\n"

# Challenge: implement a similar program using a loop and break an continue, and order the dictionary (hint: use a list)

print(separator+"jjj")
albumTuple = "Destroyer", 1976

albumDict = dict([albumTuple])

print(albumDict)

anotherAlbum = {"Unmasked": 1980}
albumDict.update(anotherAlbum)

anotherAlbum.update(albumDict)

print (albumDict)
print (anotherAlbum)

