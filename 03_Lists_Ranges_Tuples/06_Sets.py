separator = "-" * 20

farmAnimals = {"sheep", "cow", "bull"}

wildAnimals = set(["lion", "tiger", "panther"])

print(farmAnimals)
print(wildAnimals)

print(separator)

farmAnimals.add("horse")
wildAnimals.add("wolf")

print(farmAnimals)
print(wildAnimals)

print(separator)

print(farmAnimals.difference(wildAnimals))
print(wildAnimals.difference(farmAnimals))

farmAnimals.add("tiger")
farmAnimals.add("lion")
print(farmAnimals)

print(farmAnimals.symmetric_difference(wildAnimals))

farmAnimals.difference_update(wildAnimals)
print(farmAnimals)



print(separator)

farmAnimals.discard("cow")
print(farmAnimals)

print(separator)

# Using union you will notice the items do not repeat
print(farmAnimals.union(wildAnimals))
print(wildAnimals.union(farmAnimals))

print(separator)
# Creating an empty Set
# WRONG! Her you are creating a Dictionary , eg. try to use the add method
# emptySet = {}
emptySet = set()

print(emptySet)
print(len(emptySet))

emptySet.add("something")
print(len(emptySet))

print(separator)
print(separator)

message = "Batman"
vowels = "aeiou"

example = set(message)

# print(example.difference(vowels))
example.difference_update(vowels)

print(example)

