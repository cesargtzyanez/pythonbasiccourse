path = '../assets/sample.txt'

# Example 1
# file = open(path, 'r', encoding='utf-8')
#
# for line in file:
#     print(line.replace('.', '.\n'))
#
# file.close()

# Example 2
# with open(path,'r',encoding='utf-8') as file:
#     print(file.readline().replace('.', '.\n'))

# Example 3
with open(path, 'r', encoding='utf-8') as file:
    allLines = file.readlines()

for line in allLines:
    print(line.replace('.', '.\n'), end='')
