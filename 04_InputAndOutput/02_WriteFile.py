path = '../assets/write.txt'

name = input('What is your name?')

with open(path,'w',encoding='utf-8') as file:
    print("Hello, my name is {}".format(name), file=file)

print("Thank you {}".format(name))

# Challenge: using the albums example, create a program that prints the order of a client
