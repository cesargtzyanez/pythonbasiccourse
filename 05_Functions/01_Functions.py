def print_title(title, symb='-', length=10):
    separator = symb * length
    print(separator + '> ' + title + ' <' + separator)


print_title("This is my title")

# The variable separator is not in the scope
# print(separator)
