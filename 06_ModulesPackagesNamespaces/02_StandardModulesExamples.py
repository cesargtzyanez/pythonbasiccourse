import webbrowser
import time
import random
import tkinter

separator = '=' * 15 + '>'

print(separator + "Web Browser")
# webbrowser.open('https://www.google.com')

print(time.gmtime(0))
print(time.localtime(time.time()))
print(time.time())

print(separator + "Time")
now = time.localtime()
print(now)
print("Year: ", now[0], now.tm_year)
print("Year: ", now[1], now.tm_mon)
print("Year: ", now[2], now.tm_mday)

print(separator + "Random")
print(random.randint(1, 100))

print(separator + "Tkinter")

# tkinter._test()
mainWindow = tkinter.Tk()

title = input("Hello!")

mainWindow.title("Hey! Hello {}!".format(title))
mainWindow.geometry('300x300+100+100')
mainWindow.mainloop()
