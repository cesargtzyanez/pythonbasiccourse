import MyModules.myUtils as u
# Error importing
# import MyClasses.Mammal.Mammal

# Example 1 of importing
# import MyClasses.Mammal as Mammal

# Example 2 (better one)
from MyClasses.Mammal import Mammal

# Example 1 of instantiation
# myPet = Mammal.Mammal("Rex", "male", 4, 50, 25, False)

# Example 2, yes, the better one
myDog = Mammal("Rex", "male", 4, 50, 25, False)

myDog.say_hello()
myDog.run()
myDog.eat()

print(myDog.name)

myCat = Mammal("Whisky", "female", 4, 60, 30)

myCat.say_hello()
myCat.eat()
myCat.run()

u.print_block(myCat.name, myCat.name)
