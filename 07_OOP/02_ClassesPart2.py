# First, create a class for Cat, Dog and Person

from MyModules import myUtils as u
from MyClasses.Cat import Cat

tom = Cat('Tom', 'male', 25, 7)

tom.talk()
tom.say_hello()

u.print_block(tom.talk(), tom.name)
