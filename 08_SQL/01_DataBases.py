import sqlite3

db = sqlite3.connect("myDatabase.db")

db.execute("SELECT * FROM contacts")

# First example should be without the cursor
myCursor = db.cursor()
myCursor.execute("SELECT * FROM contacts")

for row in myCursor:
    print(row)
    # Example creating a dynamic message
    print("My name is {}, and my phone number is {}, email me to {}. Thank you".format(row[0], row[1], row[2]),end="\n\n")

db.close()
