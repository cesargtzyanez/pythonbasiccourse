from MyClasses.Mammal import Mammal


class Cat(Mammal):
    def __init__(self, name, genre, height, weight, awake=True):
        Mammal.__init__(self, name, genre, 4, height, weight, awake)
        self.species = "CAT"

    def talk(self):
        message = "Meowww!"
        print(message)
        return message

    def say_hello(self):
        message = "Prrr... I'm a {} cat and my name is {}".format(self.genre, self.name)
        print(message)
        return message
