class Mammal(object):
    def __init__(self, name, genre, legs, height, weight, awake=True):
        self.genre = genre
        self.name = name
        self.legs = legs
        self.awake = awake
        self.height = height
        self.weight = weight

    def say_hello(self):
        message = "Hello, my name is {}".format(self.name)
        print(message)
        return message

    def say(self, speech):
        message = speech
        print(message)
        return message

    def wake_up(self):
        self.awake = True
        message = "I'm awake"
        print(message)
        return self.awake

    def sleep(self):
        self.awake = False
        message = "I'm NOT awake"
        print(message)
        return self.awake

    def eat(self):
        message = "I'm {} and I'm eating".format(self.name)
        print(message)
        return message

    def run(self):
        message = "I'm running with my {} legs ".format(self.legs)
        print(message)
        return message
