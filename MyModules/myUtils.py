# First Version
# def print_title(title, symb='-', length=10):
#     separator = symb * length
#     print(separator + '> ' + title + ' <' + separator)


def print_title(title, symb='-', length=10):
    separator = symb * length
    finalString = separator + '> ' + title + ' <' + separator
    print(finalString)
    # This improvement is for the next functions
    return finalString


def separator(symb='-', length=20):
    print(symb * length)


def print_block(content, title='Block', symb='-', length=10):
    title = print_title(title, symb, length)
    print(content)
    # This is a first approach
    # separator(symb,length*2 + len(title) + 4)
    separator(symb, len(title))
